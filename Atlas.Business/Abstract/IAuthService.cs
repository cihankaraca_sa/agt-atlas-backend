﻿using Atlas.Core.Entities.Concrete;
using Atlas.Core.Utilities.Results;
using Atlas.Core.Utilities.Security.Jwt;
using Atlas.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace Atlas.Business.Abstract
{
    public interface IAuthService
    {
        IDataResult<User> Register(UserForRegisterDto userForRegisterDto, string password);
        IDataResult<User> Login(UserForLoginDto userForLoginDto);
        IResult UserExists(string email);

        IDataResult<AccessToken> CreateAccessToken(User user);

    }
}
