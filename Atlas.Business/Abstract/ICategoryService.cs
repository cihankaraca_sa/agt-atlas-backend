﻿using Atlas.Core.Utilities.Results;
using Atlas.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Text;

namespace Atlas.Business.Abstract
{
    public interface ICategoryService
    {

        IDataResult<Category> GetById(int categoryId);
        IDataResult<List<Category>> GetList(); 
        
        IResult Add(Category category);
        //IResult Delete(Category category);
        IResult Update(Category category);
        
    }
}
