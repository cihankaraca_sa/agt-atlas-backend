using Atlas.Core.Utilities.Results;
using Atlas.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Text;

namespace Atlas.Business.Abstract
{
    public interface IGonderiService
    {
        IDataResult<Gonderi> GetByGonderiNo(decimal gonderiNo);
        IDataResult<List<Gonderi>> GetList();
        IResult Add(Gonderi gonderi);
        IResult Delete(Gonderi gonderi);
        IResult Update(Gonderi gonderi);

        IDataResult<Gonderi> HizliGonderiArama(decimal gonderiNo);
    }
}
