﻿using Atlas.Core.Utilities.Results;
using Atlas.Core.Entities.Concrete;
using Atlas.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace Atlas.Business.Abstract
{
    public interface IUserService
    {
        List<OperationClaim> GetClaims(User user);
        //TODO : List<OperationClaimDto> GetClaims(User user);
        void Add(User user);
        User GetByMail(string email);
    }
}
