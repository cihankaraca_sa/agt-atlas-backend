﻿using Atlas.Business.Abstract;
using Atlas.Core.Utilities.Results;
using Atlas.DataAccess.Abstract;
using Atlas.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Text;
using Atlas.Business.Constants;
using System.Linq;

namespace Atlas.Business.Concrete
{
    public class CategoryManager : ICategoryService
    {

        private ICategoryDal _categoryDal;
        public CategoryManager(ICategoryDal categoryDal)
        {
            _categoryDal = categoryDal;
        } 
        public IResult Add(Category category)
        { 
            _categoryDal.Add(category);             
            return new SuccessResult(Messages.CategoryAdded);
        }
        /*
        public IResult Delete(Category category)
        {
            _categoryDal.Delete(category);
            return new SuccessResult(Messages.CategoryDeleted);
        }
        */
        public IResult Update(Category category)
        {
            _categoryDal.Update(category);
            return new SuccessResult(Messages.CategoryUpdated);
        } 
        public IDataResult<Category> GetById(int categoryId)
        {
            return new SuccessDataResult<Category>(_categoryDal.Get(p => p.CategoryId == categoryId));
        }

        public IDataResult<List<Category>> GetList()
        {
            return new SuccessDataResult<List<Category>>(_categoryDal.GetList().ToList());
        }

    }
}
