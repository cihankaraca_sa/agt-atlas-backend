using Atlas.Business.Abstract;
using Atlas.Business.BusinessAspects.Autofac;
using Atlas.Business.Constants;
using Atlas.Business.ValidationRules.FluentValidation;
using Atlas.Core.Aspects.Autofac.Caching;
using Atlas.Core.Aspects.Autofac.Logging;
using Atlas.Core.Aspects.Autofac.Performance;
using Atlas.Core.Aspects.Autofac.Transaction;
using Atlas.Core.Aspects.Autofac.Validation;
using Atlas.Core.CrossCuttingConcerns.Logging.Log4Net.Loggers;
using Atlas.Core.CrossCuttingConcerns.Validation;
using Atlas.Core.Utilities.Business;
using Atlas.Core.Utilities.Results;
using Atlas.DataAccess.Abstract;
using Atlas.Entities.Concrete; 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Atlas.Business.Concrete
{
    public class GonderiManager : IGonderiService
    {
        private IGonderiDal _gonderiDal; 
        public GonderiManager(IGonderiDal gonderiDal)
        {
            _gonderiDal = gonderiDal;            
        }
        
        //TODO: Methodlara İş Kodları gelecek. kontroller gibi.
        
        [SecuredOperation("Gonderi.Add")]
        //[ValidationAspect(typeof(GonderiValidator), Priority =1)]
        //[CacheRemoveAspect("IGonderiService.Get")]
        //[LogAspect(typeof(DatabaseLogger))]
        //[CacheAspect(duration:10)]    
        public IResult Add(Gonderi gonderi)
        {

            _gonderiDal.Add(gonderi);
            return new SuccessResult(Messages.GonderiAdded);
        }

        [SecuredOperation("Gonderi.Delete")]
        public IResult Delete(Gonderi gonderi)
        {
              _gonderiDal.Delete(gonderi);
            return new SuccessResult(Messages.GonderiDeleted);
        }

        [SecuredOperation("Gonderi.Update")]
        public IResult Update(Gonderi gonderi)
        {
            _gonderiDal.Update(gonderi);
           return new SuccessResult(Messages.GonderiUpdated);
        }
        
        [SecuredOperation("Gonderi.GetByGonderiNo")]
        public IDataResult<Gonderi> GetByGonderiNo(decimal gonderiNo)
        {            
            return new SuccessDataResult<Gonderi>(_gonderiDal.Get(p => p.GonderiNo == gonderiNo));
        }

        [SecuredOperation("Gonderi.GetByGonderiNo")]
        public IDataResult<Gonderi> GetByMusOzelKod(string musOzelKod)
        {
            return new SuccessDataResult<Gonderi>(_gonderiDal.Get(p => p.MusOzelKod == musOzelKod));
        }

        [SecuredOperation("Gonderi.GetList")]    
        public IDataResult<List<Gonderi>> GetList()
        {            
            return new SuccessDataResult<List<Gonderi>>(_gonderiDal.GetList().ToList());
        }

 
        [SecuredOperation("Gonderi.HizliGonderiArama")]
        public IDataResult<Gonderi> HizliGonderiArama(decimal gonderiNo)
        {
            return new SuccessDataResult<Gonderi>(_gonderiDal.HizliGonderiArama( gonderiNo));
        }

    }
}
