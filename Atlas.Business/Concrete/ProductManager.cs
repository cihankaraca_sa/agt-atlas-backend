﻿using Atlas.Business.Abstract;
using Atlas.Business.BusinessAspects.Autofac;
using Atlas.Business.Constants;
using Atlas.Business.ValidationRules.FluentValidation;
using Atlas.Core.Aspects.Autofac.Caching;
using Atlas.Core.Aspects.Autofac.Logging;
using Atlas.Core.Aspects.Autofac.Performance;
using Atlas.Core.Aspects.Autofac.Transaction;
using Atlas.Core.Aspects.Autofac.Validation;
using Atlas.Core.CrossCuttingConcerns.Logging.Log4Net.Loggers;
using Atlas.Core.CrossCuttingConcerns.Validation;
using Atlas.Core.Utilities.Business;
using Atlas.Core.Utilities.Results;
using Atlas.DataAccess.Abstract;
using Atlas.Entities.Concrete; 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Atlas.Business.Concrete
{
    public class ProductManager : IProductService
    {
        private IProductDal _productDal;
        private ICategoryService _categoryService;
        public ProductManager(IProductDal productDal, ICategoryService categoryService)
        {
            _productDal = productDal;
            _categoryService = categoryService;
        }
        //TODO: Methodlara İş Kodları gelecek. kontroller gibi.
        [ValidationAspect(typeof(ProductValidator), Priority =1)]
        [CacheRemoveAspect("IProductService.Get")]       // Birkaçtane eklenip farklı cache ler silinebilir.
    
        public IResult Add(Product product)
        {

            IResult result = BusinessRules.Run(CheckIfProductNameExists(product.ProductName),
                CheckIfCategoryIsEnabled(product.CategoryId));
            if (result != null)
            {
                return result;
            }
            _productDal.Add(product);
            return new SuccessResult(Messages.ProductAdded);
        }

        private IResult CheckIfProductNameExists(string productName)
        {
            
            var result = _productDal.GetList(p => p.ProductName == productName).Any();
            if (result)
            {
                return new ErrorResult(Messages.ProductNameAlreadyExists);
            }
            return new SuccessResult();
        }

        private IResult CheckIfCategoryIsEnabled(int categoryId)
        {

            
            if (_categoryService.GetById(categoryId) == null)
            {
                return new ErrorResult(Messages.CategoryNotEnabled);
            }
            return new SuccessResult();
        }
        public IResult Delete(Product product)
        {
              _productDal.Delete(product);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public IResult Update(Product product)
        {
            _productDal.Update(product);
           return new SuccessResult(Messages.ProductUpdated);
        }

        public IDataResult<Product> GetById(int productId)
        {
            
            return new SuccessDataResult<Product>(_productDal.Get(p => p.ProductId == productId));
        }


        public IDataResult<Product> GetByIdDecoded(int productId)
        {
            //TODO DECODE GONDERİ DATA
            //Call DecodedHelper
            return new SuccessDataResult<Product>(_productDal.Get(p => p.ProductId == productId));
        }

        //TODO:Performans izlenecek methodlara eklenecek

        [PerformanceAspect(5)]
        public IDataResult<List<Product>> GetList()
        {
            Thread.Sleep(6);
            return new SuccessDataResult<List<Product>>(_productDal.GetList().ToList());
        }

        [SecuredOperation("Product.List,Admin,Developer")]
        [LogAspect(typeof(DatabaseLogger))]
        [CacheAspect(duration:10)]
        public IDataResult<List<Product>> GetListByCategory(int categoryId)
        {
            return new SuccessDataResult<List<Product>>(_productDal.GetList(p => p.CategoryId== categoryId).ToList());
        }

        [TransactionScopeAspect]
        public IResult TransactionalOperation(Product product)
        {
            _productDal.Update(product);
            _productDal.Add(product);

            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
