﻿using Atlas.Business.Abstract;
using Atlas.Business.Constants;
using Atlas.Core.Utilities.Results;
using Atlas.DataAccess.Abstract;
using Atlas.Core.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Text;

namespace Atlas.Business.Concrete
{
    public class UserManager : IUserService
    {
        IUserDal _userDal;
        public UserManager(IUserDal userDal)
        {
            _userDal = userDal;
        }
        public List<OperationClaim> GetClaims(User user)
        {
            return _userDal.GetClaims(user);
        }
        public void Add(User user)
        {
            _userDal.Add(user);
            //return new SuccessResult(Messages.CategoryAdded);
        }

        public User GetByMail(string email)
        {
            return _userDal.Get(u => u.Email == email);
        }

        //TODO:Kullanıcı Unmask Yetkisi olan alanlar için method

        /* //TODO : 
        
         
        public List<OperationClaimDto> GetClaims(User user)
        {
            return _userDal.GetClaims(user);
        }
         * */
    }
}
