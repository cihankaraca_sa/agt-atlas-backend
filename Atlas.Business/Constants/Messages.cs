﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atlas.Business.Constants
{
    public static class Messages
    {
        public static string ProductAdded = "Ürün başarıyla eklendi";
        public static string ProductDeleted = "Ürün başarıyla silindi";
        public static string ProductUpdated = "Ürün başarıyla güncellendi";
        public static string ProductNameAlreadyExists = "Ürün ismi zaten mevcut";


        public static string CategoryAdded = "Kategori başarıyla eklendi";
        public static string CategoryDeleted = "Kategori başarıyla silindi";
        public static string CategoryUpdated = "Kategori başarıyla güncellendi";
        public static string CategoryNotEnabled = "Bu Kategori ye ürün eklenemez";


        public static string UserAdded = "Kullanıcı başarıyla eklendi";
        public static string UserDeleted = "Kullanıcı başarıyla silindi";
        public static string UserUpdated = "Kullanıcı başarıyla güncellendi";
        public static string UserNotFound = "Kullanıcı bulunamadı";
        public static string PasswordError = "Şifre Hatalı!";
        public static string SuccessfulLogin = "Sisteme Giriş başarılı";
        public static string UserAlreadyExists = "Bu kullanıcı zaten mevcut";
        public static string UserRegistered = "Kullanıcı başarıyla kaydedildi";
        public static string AccessTokenCreated = "Access Token başarıyla oluşturuldu";



        public static string ValidationNotEmpty = "{0} boş olamaz";
        public static string ValidationLen = "{0} karakter olmalı"; 
        public static string ValidationGreaterThanOrEqualTo = "{0} büyük olmalı";
        public static string ValidationStartWith = "{0} ile başlamalı";
        public static string ValidationType = "Hatalı";

        public static string AuthorizationDenied = "Yetkiniz yok!";

        
        public static string GonderiUpdated = "Gonderi Updated";
        public static string GonderiAdded = "Gonderi Added";
        public static string GonderiDeleted = "Gonderi Deleted";

        //TODO:ADD-MessageLine

    }
}
