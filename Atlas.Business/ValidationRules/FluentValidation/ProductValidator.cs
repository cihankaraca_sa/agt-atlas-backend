﻿using Atlas.Business.Constants;
using Atlas.Entities.Concrete;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Atlas.Business.ValidationRules.FluentValidation
{
    public class ProductValidator:AbstractValidator<Product>
    {
        public ProductValidator()
        {
            RuleFor(p => p.ProductName).NotEmpty().WithMessage( String.Format(Messages.ValidationNotEmpty,"Ürün Adı"));
            RuleFor(p => p.ProductName).Length(2,30).WithMessage(String.Format(Messages.ValidationLen, "Ürün Adı 2-30") );
            RuleFor(p => p.UnitPrice).NotEmpty().WithMessage(String.Format(Messages.ValidationNotEmpty, "Ürün Tutarı"));
            RuleFor(p => p.UnitPrice).GreaterThanOrEqualTo(1).WithMessage(String.Format(Messages.ValidationGreaterThanOrEqualTo, "Ürün tutarı 1 den"));
            RuleFor(p => p.UnitPrice).GreaterThanOrEqualTo(1).When(p=>p.CategoryId==1).WithMessage(String.Format(Messages.ValidationGreaterThanOrEqualTo, "Ürün tutarı 1 den"));
            RuleFor(p => p.ProductName).Must(StartWithA).WithMessage(String.Format(Messages.ValidationStartWith, "Ürün Adı 'A' "));

        }

        private bool StartWithA(string arg)
        {
            return arg.StartsWith("A");
        }
    }
}
