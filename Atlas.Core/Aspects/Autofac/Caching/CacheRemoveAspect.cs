﻿
using Atlas.Core.CrossCuttingConcerns.Caching;
using Atlas.Core.Utilities.Interceptors;
using Atlas.Core.Utilities.IoC;
using Castle.DynamicProxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Extensions.DependencyInjection;

namespace Atlas.Core.Aspects.Autofac.Caching
{
    public class CacheRemoveAspect : MethodInterception
    {
        private string _pattern;
        private ICacheManager _cacheManager;
        public CacheRemoveAspect(string pattern)
        {
            _pattern = pattern;
            _cacheManager = ServiceTool.ServiceProvider.GetService<ICacheManager>();
        }
        protected override void OnSuccess(IInvocation invocation)
        {
         
            _cacheManager.RemoveByPattern(_pattern);

        }
    }
}
