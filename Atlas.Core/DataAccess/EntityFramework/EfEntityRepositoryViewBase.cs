﻿using Atlas.Core.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Atlas.Core.DataAccess;
namespace Atlas.Core.DataAccess.EntityFramework
{
    public class EfEntityRepositoryViewBase<TEntity, TContext> : Atlas.Core.DataAccess.IEntityRepositoryView<TEntity>
        where TEntity : class, IEntity, new()
        where TContext : DbContext, new()
    {
        

        public TEntity Get(Expression<Func<TEntity, bool>> filter = null)
        {
            using (var context = new TContext())
            {
                var entity = context.Set<TEntity>().SingleOrDefault(filter);
                return entity;
            }
        }

        public List<TEntity> GetList(Expression<Func<TEntity, bool>> filter = null)
        {
            using (var context = new TContext())
            {
                IQueryable<TEntity> query = context.Set<TEntity>();

                return filter == null
                    ? query.ToList()
                    : query.Where(filter).ToList();
            }
        }
         
    }
}
