﻿* İsimlendirme Kuralları
- public Property, Method ve Nesne isimleri Pascal case 
	Örnekler:-> Product, ProductManager, Product GetById(int productId){}
- Method parametreleri Camel Case  
	Örnekler:-> Delete(int productId)
- private değişkenler "_" alt çizgi ile başlar. 
	Örnekler:-> private Product _product;
- Veritabanından gelecek nesne bir View ise IXxxxService interface içinde Add, Delete, Update yazılmaz.
- Cros Cutting Conserns -> Validation, Cache, Log, Performance, Aut, Transaction
- AOP - Yazılım geliştirme yaklaşımı -> Aspect Oriented Programming
	Örneğin : Operasyon/Method başında validation yapılır.
- Messages ve AspectMessages DB den gelecek