﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atlas.Core.Utilities.HandleException
{
    public static class HandleException
    {
        public static void HandleErrorException(Action action)
        {
            //TODO: Kullanılabilir mi bakılacak 
            // Transaction Loglama gibi CrossCuttingConcers lerde kullanılabilir.
            // Action Delegasyonu İle Hata Yakalama
            /*
             Kullanım : 
            HandleException.HandleErrorException(() =>
            {
                _categoryDal.Add(category);
            });
             */
            try
            {
                action.Invoke();
            }
            catch (Exception exception)
            {
                //Oluşan Exception burada yakalanır
                throw exception;
            }
        }
    }
}
