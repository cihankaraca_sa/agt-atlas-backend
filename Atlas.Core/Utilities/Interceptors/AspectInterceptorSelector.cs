﻿using Atlas.Core.Aspects.Autofac.Exception;
using Atlas.Core.CrossCuttingConcerns.Logging.Log4Net.Loggers;
using Castle.DynamicProxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Atlas.Core.Utilities.Interceptors
{
    public class AspectInterceptorSelector : IInterceptorSelector
    {
        public IInterceptor[] SelectInterceptors(Type type, MethodInfo method, IInterceptor[] interceptors)
        {
            var classAttributes = type.GetCustomAttributes<MethodInterceptionBaseAttribute>(true).ToList();
            var methodAttributes = type.GetMethod(method.Name)
                .GetCustomAttributes<MethodInterceptionBaseAttribute>(true).ToList();
            classAttributes.AddRange(methodAttributes);
            //classAttributes.Add(new ExceptionLogAspect(typeof(DatabaseLogger)));
             classAttributes.Add(new ExceptionLogAspect(typeof(JsonFileLogger)));// ikisi de açılabilir.

            return classAttributes.OrderBy(x => x.Priority).ToArray();
        }
    }
}
