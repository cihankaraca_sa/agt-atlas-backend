﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atlas.Core.Utilities.Results
{
    public class SuccessResult : Result
    {
        public SuccessResult(string message) : base(true,message) //KODLAMA:
        {
            
        }
        public SuccessResult():base(true)
        {
            
        }
    }
}
