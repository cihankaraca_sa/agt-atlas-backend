﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atlas.Core.Utilities.Utility
{
    public static class ProjectUtility
    {
        public static List<T> BuildList<T>(T[] items) where T:class, new()
        {
            return new List<T>(items);
        }
    }
}
