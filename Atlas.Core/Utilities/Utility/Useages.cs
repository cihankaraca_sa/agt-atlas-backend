﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atlas.Core.Utilities.Utility
{
    class Useages
    {
        public delegate void MyDelegate();
        public delegate void MyDelegate2(string a);

        //Event
        public delegate void CustomerControl();


        [Obsolete("Bu Method eski kullanmayın. ObsoleteMethodNew Methodunu kullanın")]
        public static void ObsoleteMethod()
        {
            Customer customer = new Customer();
            MyDelegate _delegate = customer.CheckExists;
            _delegate += customer.CheckValues;

            MyDelegate2 _delegate2 = customer.CheckName;
            _delegate2 += customer.CheckLastName;


            Func<int, string> add = customer.GetName;
            add += customer.GetLastName;

            Func<int> getRandomNumber = delegate ()
            {
                Random random = new Random();
                return random.Next(1, 100);
            };

            Func<int> getRandomNumber2 = ()=>new Random().Next(1, 100);

            //Event e abone olduk. belirli bir işlemde/durumda çalışmasını istedik
            customer.CustomerControlEvent += Customer_CustomerControlEvent;

        }

        private static void Customer_CustomerControlEvent()
        {
            throw new NotImplementedException();
        }

        public static void ObsoleteMethodNew()
        {

            ObsoleteMethod();
        }
        [ToTable("Customer")]
        class Customer
        {
            public event CustomerControl CustomerControlEvent;
            public string GetName(int id)
            {
                return "name";
            }
            public string GetLastName(int id)
            {
                return "lastname";
            }
            public void CheckExists()
            {

            }

            public void CheckValues()
            {

            }

            public void CheckName(string name)
            {

            }
            public void CheckLastName(string lastName)
            {

            }
        }
        [AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface)]
        class ToTableAttribute : Attribute
        {

            string _tableName;
            public ToTableAttribute(string tableName)
            {
                _tableName = tableName;
            }
        }


    }
}
