﻿using Atlas.Core.DataAccess;
using Atlas.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Text;

namespace Atlas.DataAccess.Abstract
{
    public interface ICategoryDal:IEntityRepository<Category>
    {
    }
}
