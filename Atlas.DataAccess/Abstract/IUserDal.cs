﻿using Atlas.Core.DataAccess;
using Atlas.Core.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Text;

namespace Atlas.DataAccess.Abstract
{
    public interface IUserDal:IEntityRepository<User>
    {
        List<OperationClaim> GetClaims(User user);
        //->TODO List<OperationClaimDto> GetClaims(User user);
    }
}
