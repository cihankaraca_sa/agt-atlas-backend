﻿using Atlas.Entities.Concrete;
using Atlas.Core.Entities.Concrete;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Atlas.DataAccess.Concrete.EntityFramework.Contexts
{
    public class AtlasContext:DbContext
    {

        private string connectionString=Properties.Resources.connectionString;
        string _ConnectionString;
        public AtlasContext(string ConnectionString)
        {
            _ConnectionString =  ConnectionString;
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_ConnectionString);
        }
        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<OperationClaim> OperationClaims { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserOperationClaim> UserOperationClaims { get; set; }
        //TODO:ADD-DBSET

        

    }
}
