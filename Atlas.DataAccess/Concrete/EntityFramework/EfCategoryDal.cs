﻿using Atlas.Core.DataAccess.EntityFramework;
using Atlas.DataAccess.Abstract;
using Atlas.DataAccess.Concrete.EntityFramework.Contexts;
using Atlas.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Text;

namespace Atlas.DataAccess.Concrete.EntityFramework
{
    public class EfCategoryDal: EfEntityRepositoryBase<Category,AtlasContext>,ICategoryDal
    {       

    }
}
