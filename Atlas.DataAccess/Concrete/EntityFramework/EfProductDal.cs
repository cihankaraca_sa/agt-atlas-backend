﻿using Atlas.Core.DataAccess.EntityFramework;
using Atlas.DataAccess.Abstract;
using System;
using System.Collections.Generic;
using System.Text;
using Atlas.Core.DataAccess;
using Atlas.Entities.Concrete;
using Atlas.DataAccess.Concrete.EntityFramework.Contexts;

namespace AGT.DataAccess.Concrete.EntityFramework
{
    public class EfProductDal :   EfEntityRepositoryBase<Product,AtlasContext>, IProductDal
    {
    }
}
