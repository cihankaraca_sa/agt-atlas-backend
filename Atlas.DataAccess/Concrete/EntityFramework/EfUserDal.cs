﻿using Atlas.Core.DataAccess.EntityFramework;
using Atlas.DataAccess.Abstract;
using Atlas.DataAccess.Concrete.EntityFramework.Contexts;
using Atlas.Core.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Atlas.DataAccess.Concrete.EntityFramework
{
    public class EfUserDal : EfEntityRepositoryBase<User, AtlasContext>, IUserDal
    {
        public List<OperationClaim> GetClaims(User user) 
        {
            using (var context = new AtlasContext() )
            {
                var result = from operationClaim in context.OperationClaims
                             join userOperationClaim in context.UserOperationClaims
                             on operationClaim.Id equals userOperationClaim.OperationClaimId
                             where userOperationClaim.UserId == user.Id
                             select new OperationClaim { Id = operationClaim.Id, Name = operationClaim.Name };

                return result.ToList();
            }
        }

        public List<OperationClaim> GetClaimsMobile(User user)
        { 
            using (var context = new AtlasContext(Properties.Resources.connectionStringUnMasked))
            {

                
                var result = from operationClaim in context.OperationClaims
                             join userOperationClaim in context.UserOperationClaims
                             on operationClaim.Id equals userOperationClaim.OperationClaimId
                             where userOperationClaim.UserId == user.Id
                             select new OperationClaim { Name = operationClaim.Name };

                return result.ToList();
            }

             
        }

        /*
         //TODO:
         
        public List<OperationClaim> GetClaims(User user)
        {
            using (var context = new AtlasContext() )
            {
                var result = from operationClaim in context.OperationClaims
                             join userOperationClaim in context.UserOperationClaims
                             on operationClaim.Id equals userOperationClaim.OperationClaimId
                             where userOperationClaim.UserId == user.Id
                             select new OperationClaimDto { Id = operationClaim.Id, Name = operationClaim.Name };

                return result.ToList();
            }
        }
         
         */
    }
}
