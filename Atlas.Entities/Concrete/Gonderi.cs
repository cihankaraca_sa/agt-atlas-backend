using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Atlas.Core.Entities;

namespace Atlas.Entities.Concrete
{

    [Table("Gonderiler")]
    public class Gonderi:IEntity
    {
		        
        [Column("iD")]
        public int Id { get; set; }
        [Column("Parti_iD")]
        public int PartiId { get; set; }
        [Column("Seri_No")]
        public short SeriNo { get; set; }
        [Column("Gonderi_No")]
        public decimal GonderiNo { get; set; }
        [Column("Alici_Ad")]
        public string AliciAd { get; set; }
        [Column("Alici_Soyad")]
        public string AliciSoyad { get; set; }
        [Column("islem1_iD")]
        public int Islem1Id { get; set; }
        [Column("islem1_Gun")]
        public DateTime Islem1Gun { get; set; }
        [Column("islem1_saat")]
        public short Islem1Saat { get; set; }
        [Column("islem1_Dagitici_iD")]
        public int Islem1DagiticiId { get; set; }
        [Column("Teslim_Alan")]
        public string TeslimAlan { get; set; }
        [Column("Teslim_Alan_Tel")]
        public string TeslimAlanTel { get; set; }
        [Column("islem2_iD")]
        public int Islem2Id { get; set; }
        [Column("islem2_Gun")]
        public DateTime Islem2Gun { get; set; }
        [Column("islem2_saat")]
        public short Islem2Saat { get; set; }
        [Column("islem2_Dagitici_iD")]
        public int Islem2DagiticiId { get; set; }
        [Column("Notlar")]
        public string Notlar { get; set; }
        [Column("Dagitim_Bolgeleri_iD")]
        public int DagitimBolgeleriId { get; set; }
        [Column("Update_Tarih")]
        public DateTime UpdateTarih { get; set; }
        [Column("Update_User")]
        public string UpdateUser { get; set; }
        [Column("Ana_Bolge_iD")]
        public int AnaBolgeId { get; set; }
        [Column("Adres")]
        public string Adres { get; set; }
        [Column("Fiyat")]
        public int Fiyat { get; set; }
        [Column("Tahsil_Edilen")]
        public int TahsilEdilen { get; set; }
        [Column("Kontrol_Parti_iD")]
        public int KontrolPartiId { get; set; }
        [Column("il")]
        public string Il { get; set; }
        [Column("Gon_Tur")]
        public short GonTur { get; set; }
        [Column("Urun_Kod_Old")]
        public string UrunKodOld { get; set; }
        [Column("Urun_Kod")]
        public string UrunKod { get; set; }
        [Column("imza")]
        public string Imza { get; set; }
        [Column("Kimlik")]
        public string Kimlik { get; set; }
        [Column("AdSoyTel")]
        public string Adsoytel { get; set; }
        [Column("iade_Tarih")]
        public DateTime IadeTarih { get; set; }
        [Column("Aktarildi")]
        public short Aktarildi { get; set; }
        [Column("iade_Seri_No")]
        public string IadeSeriNo { get; set; }
        [Column("Mus_Ozel_Kod")]
        public string MusOzelKod { get; set; }
        [Column("Teslim_Alan_Tel1")]
        public string TeslimAlanTel1 { get; set; }
        [Column("Teslim_Alan_Tel2")]
        public string TeslimAlanTel2 { get; set; }
        [Column("Not_Dag_Tar")]
        public DateTime NotDagTar { get; set; }
        [Column("Posta_Kargo_Fiyat")]
        public int PostaKargoFiyat { get; set; }
        [Column("image")]
        public string Image { get; set; }
        [Column("kargo_gonderim_tar")]
        public DateTime KargoGonderimTar { get; set; }
        [Column("Koli_No")]
        public string KoliNo { get; set; }
        [Column("Koli_id")]
        public int KoliId { get; set; }
        [Column("Parti_Alt_Kod")]
        public int PartiAltKod { get; set; }
        [Column("Tahakkuk_Edilmeyen_Prim")]
        public int TahakkukEdilmeyenPrim { get; set; }
        [Column("NBSDolan")]
        public DateTime Nbsdolan { get; set; }
        [Column("Zarf_Adresi")]
        public string ZarfAdresi { get; set; }
        [Column("Teslim_Alan_Ad")]
        public string TeslimAlanAd { get; set; }
        [Column("Teslim_Alan_Soyad")]
        public string TeslimAlanSoyad { get; set; }
        [Column("GeriDonusBilgisi")]
        public string Geridonusbilgisi { get; set; }
        [Column("EMail")]
        public string Email { get; set; }
        [Column("OzelNot")]
        public string Ozelnot { get; set; }
        [Column("GuncelAdres")]
        public string Gunceladres { get; set; }
        [Column("iade_check")]
        public bool IadeCheck { get; set; }
        [Column("Kanit")]
        public string Kanit { get; set; }
        [Column("iade_paket_id")]
        public int IadePaketId { get; set; }
        [Column("Teslim_Alan_Tel3")]
        public string TeslimAlanTel3 { get; set; }
        [Column("yas_grubu")]
        public string YasGrubu { get; set; }
        [Column("mekan")]
        public string Mekan { get; set; }
        [Column("cinsiyet")]
        public string Cinsiyet { get; set; }
        [Column("onzimmet")]
        public byte Onzimmet { get; set; }
        [Column("Tbl_iade_Paketleri_id")]
        public int TblIadePaketleriId { get; set; }
        [Column("ugt_kodu")]
        public byte UgtKodu { get; set; }
        [Column("ugt_Uptd_usr")]
        public string UgtUptdUsr { get; set; }
        [Column("yoklama_tarihi")]
        public DateTime YoklamaTarihi { get; set; }
        [Column("ileri_tar_randevu")]
        public bool IleriTarRandevu { get; set; }
        [Column("teslim_iade_gunu")]
        public short TeslimIadeGunu { get; set; }
        [Column("talimat_konusu")]
        public string TalimatKonusu { get; set; }
        [Column("ugt_update_tarih")]
        public DateTime UgtUpdateTarih { get; set; }
        [Column("lkstarih")]
        public DateTime Lkstarih { get; set; }
        [Column("kayip_aciklama")]
        public string KayipAciklama { get; set; }
        [Column("yoklama_Update_User")]
        public string YoklamaUpdateUser { get; set; }
        [Column("sehir_merkezi_disi")]
        public byte SehirMerkeziDisi { get; set; }
        [Column("iadeGonderiDurumu")]
        public byte Iadegonderidurumu { get; set; }
        [Column("OdemeliGonderiOnayi")]
        public DateTime Odemeligonderionayi { get; set; }
        [Column("ZiyaretSayisi")]
        public byte Ziyaretsayisi { get; set; }
        [Column("old_parti_id")]
        public int OldPartiId { get; set; }
        [Column("iade_grup_id")]
        public int IadeGrupId { get; set; }
        [Column("liste_basim_tarihi")]
        public DateTime ListeBasimTarihi { get; set; }
        [Column("ek_gonderi")]
        public bool EkGonderi { get; set; }
        [Column("firma_adi1")]
        public string FirmaAdi1 { get; set; }
        [Column("firma_adi2")]
        public string FirmaAdi2 { get; set; }
        [Column("guvenlik_kodu")]
        public string GuvenlikKodu { get; set; }
        [Column("mus_ref_no")]
        public string MusRefNo { get; set; }
        [Column("islem_adresi_id")]
        public short IslemAdresiId { get; set; }
        [Column("ilk_ugrama_tarihi")]
        public DateTime IlkUgramaTarihi { get; set; }
        [Column("zimmet_kodu")]
        public short ZimmetKodu { get; set; }
        [Column("zimmet_tarihi")]
        public DateTime ZimmetTarihi { get; set; }
        [Column("dagitim_durumu_kodu")]
        public short DagitimDurumuKodu { get; set; }
        [Column("gonderi_pks_tarihi")]
        public DateTime GonderiPksTarihi { get; set; }
        [Column("gps_id")]
        public int GpsId { get; set; }
        [Column("adres1_tipi")]
        public short Adres1Tipi { get; set; }
        [Column("adres2_tipi")]
        public short Adres2Tipi { get; set; }
        [Column("Alici_AdSoyad")]
        public string AliciAdsoyad { get; set; }
        [Column("Tes_Sube_Kodu")]
        public int TesSubeKodu { get; set; }
        [Column("Tes_Sube_Ad")]
        public string TesSubeAd { get; set; }
        [Column("TCKN")]
        public string Tckn { get; set; }
        [Column("tahsilat_tutari")]
        public decimal TahsilatTutari { get; set; }
        [Column("tahsilat_durumu")]
        public byte TahsilatDurumu { get; set; }

    }
}
