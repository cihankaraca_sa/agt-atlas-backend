﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Atlas.Core.Entities;

namespace Atlas.Entities.Concrete
{    
    public class Product:IEntity
    {               
        public int  ProductId{ get; set; }        
        public string ProductName { get; set; }
        public int CategoryId { get; set; }
        public string QuantityPerUnit { get; set; }
        public decimal UnitPrice { get; set; }
        public short UnitsInStock { get; set; }
    }

/*
Gonderi->IEntity

GonderiViewDto 
GonderiDto

GonderiNo
PartiNo
*/
}
