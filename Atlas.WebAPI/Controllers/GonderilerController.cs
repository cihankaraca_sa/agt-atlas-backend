using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Atlas.Business.Abstract;
using Atlas.Entities.Concrete;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc; 

namespace Atlas.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GonderilerController : ControllerBase
    {
        private IGonderiService _gonderiService;
        public GonderilerController(IGonderiService gonderiService)
        {
            _gonderiService = gonderiService;
        }
        [HttpGet("getall")]
        //[Authorize(Roles = "Gonderi.List")]
        public IActionResult GetList()
        { 
            var result = _gonderiService.GetList();
            if(result.Success)
            {
                return Ok(result.Data);
            }
            return BadRequest(result.Message);
        }

        [HttpGet("getbyid")]
        public IActionResult GetByiD(int iD iD)
        {
            var result = _gonderiService.GetByiD(iD);
            if (result.Success)
            {
                return Ok(result.Data);
            }
            return BadRequest(result.Message);
        }

        [HttpPost("add")]
        public IActionResult Add(Gonderi gonderi)
        {
            var result = _gonderiService.Add(gonderi);
            if (result.Success)
            {
                return Ok(result.Message);
            }
            return BadRequest(result.Message);
        }

        [HttpPost("update")]
        public IActionResult Update(Gonderi gonderi)
        {
            var result = _gonderiService.Update(gonderi);
            if (result.Success)
            {
                return Ok(result.Message);
            }
            return BadRequest(result.Message);
        }


        [HttpPost("delete")]
        public IActionResult Delete(Gonderi gonderi)
        {
            var result = _gonderiService.Delete(gonderi);
            if (result.Success)
            {
                return Ok(result.Message);
            }
            return BadRequest(result.Message);
        }
    }
}